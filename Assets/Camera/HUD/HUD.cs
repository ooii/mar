﻿using UnityEngine;
using System.Collections;

public enum Operation {
	none,
	MatNormal,
	MatBouncy,
	MatMagnetic,
	ShpSphere,
	ShpBox,
	ShpRamp,
	ReverseDirection
};

[System.Serializable]
public class Button {
	public int count;
	public Operation operation;
	public Texture texture, texture_selected, texture_disabled;
}

[System.Serializable]
public class MarData {
	public BallSpawner spawner = null;
	public WinConditions end = null;
	public int balls = 0, spawned = 0;
	public int required = 0, saved = 0;
	
	public void PullMarData() {
		balls = spawner.num_balls;
		spawned = spawner.num_spawned;
		required = end.minNumSaved;
		saved = end.numSaved;
	}
}

public class HUD : MonoBehaviour {
	public Button[] buttons;
	public Operation operation = Operation.none;
	public GUISkin skin;
	public Texture disabled_background;
	public Texture reset_icon;
	public Texture next_icon, next_icon_dark;
	public float LevelNumber;
	public MarData marData;
    public bool levelFinished = false;
	
	public void Start() {
		if (marData.spawner == null)
	    {
	        Debug.LogError("Parameter cannot be null: marData.spawner");
			Debug.Break();
	    }
		if (marData.end == null)
	    {
	        Debug.LogError("Parameter cannot be null: marData.end");
			Debug.Break();
	    }
	}
	
	public void Use() {
		for (int i = 0; i<buttons.Length; ++i) {
			if (operation == buttons[i].operation) {
				buttons[i].count --;
				if (buttons[i].count <= 0) {
					operation = Operation.none;
				}
			}
		}
	}
	
	void OnGUI() {
        if (levelFinished) {
            Vector2 Center = new Vector2(Screen.width/2, Screen.height/2);
            float boxHeight = 200;
            float boxWidth = 300;
            float textPadding = 50;
            float buttonXOffset = 10;
            float buttonYOffset = 10;
            float buttonWidth = 192;
            float buttonHeight = 64;
            
            GUI.Box(new Rect(Center.x - boxWidth/2, Center.y - boxHeight/2, boxWidth, boxHeight), marData.saved >= marData.required ? "Success" : "Failure");
            GUI.TextArea(new Rect(Center.x - (boxWidth - textPadding)/2, Center.y - (boxHeight - textPadding)/2, boxWidth - textPadding, boxHeight - textPadding), "You Saved: " + marData.saved + "\nYou needed: " + marData.required);
            if (GUI.Button(new Rect(Center.x + (boxWidth - textPadding)/2 + buttonXOffset, Center.y + (boxWidth - textPadding)/2 + buttonYOffset, buttonWidth, buttonHeight), "Continue", skin.button)) {
                Application.LoadLevel(Application.loadedLevel + 1);
            }
        }
        else {
            float boxheight = 84;
            float buttonWidth = 64;
            float buttonPadding = 16;
            float buttonSpacing = buttonWidth + buttonPadding;
            float buttonHeight = 64;
            GUI.Box(new Rect(0,Screen.height - boxheight, Screen.width, Screen.height), "", skin.box);
            
            for (int i = 0; i<buttons.Length; ++i) {
                float x,y,w,h;
                x = buttonSpacing * i + buttonPadding;
                y = Screen.height - 74;
                w = buttonWidth;
                h = buttonHeight;
                
                
                if (buttons[i].count == 0) {
                    GUIStyle style = new GUIStyle();
                    GUI.Box(new Rect(x,y,w,h),disabled_background, style);
                    GUI.Box(new Rect(x+12,y+12,w-32,h-32),buttons[i].texture_disabled,style);
                } else {
                    if (GUI.Toggle(new Rect(x,y,w,h),operation == buttons[i].operation,
                            operation == buttons[i].operation ? buttons[i].texture_selected : buttons[i].texture,
                            skin.toggle)) {
                        if (buttons[i].count > 0) {
                            operation = buttons[i].operation;
                        } else {
                            operation = Operation.none;
                        }
                    }
                }
                if (buttons[i].count >= 10) {
                    GUI.Label(new Rect(x+48,y+44,w,h), buttons[i].count.ToString(), skin.label);
                } else {
                    GUI.Label(new Rect(x+51,y+44,w,h), buttons[i].count.ToString(), skin.label);
                }
            }
            
            marData.PullMarData();
            
            GUI.TextArea(
                new Rect(Screen.width - buttonSpacing * 5,
                        Screen.height - 74,
                        buttonWidth * 2 + buttonPadding,
                        buttonHeight),
                "Mar: " + marData.spawned + " / " + marData.balls + "\n" +
                "Saved: " + marData.saved + " / " + marData.required,
                skin.textArea);
            
            if (GUI.Toggle(
                new Rect(Screen.width - buttonSpacing * 3,
                    Screen.height - 74,
                    buttonWidth,
                    buttonHeight),
                false,
                    reset_icon,
                    skin.toggle)) {
                Application.LoadLevel(Application.loadedLevel);
            }
            
            if (GUI.Toggle(
                new Rect(Screen.width - buttonSpacing * 2,
                    Screen.height - 74,
                    buttonWidth,
                    buttonHeight),
                false,
                    (marData.saved >= marData.required) ? next_icon : next_icon_dark,
                    skin.toggle)) {
                if (marData.saved >= marData.required) {
                    foreach (Light light in FindObjectsOfType(typeof(Light))) {
                        if (light.type == LightType.Spot) {
                            light.enabled = true;
                        }
                        else {
                            light.enabled = false;
                        }
                    }
                    Camera.main.backgroundColor = new Color(0,0,0,0);
                    levelFinished = true;
                }
            }
            
            //GUI.TextArea(
            //    new Rect(Screen.width - buttonWidth - buttonPadding,
            //            Screen.height - 74,
            //            buttonWidth,
            //            buttonHeight),
            //    "Level\n" + LevelNumber,
            //    skin.textArea);
            if(GUI.Button(
                new Rect(Screen.width - buttonWidth - buttonPadding,
                        Screen.height - 74,
                        buttonWidth,
                        buttonHeight),
                "Level\n" + LevelNumber,
                skin.textArea)) {
                // load the main menu
                Application.LoadLevel(0);
            }
        }
	}
}
