﻿using UnityEngine;
using System.Collections;

[System.Serializable]
enum MenuState {
	Main,
	Credits,
	Options,
	Tutorial0,
	Tutorial1,
	Tutorial2,
	Tutorial3
};

public class MainMenu : MonoBehaviour {
	public GUISkin skin;
	MenuState state = MenuState.Main;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	}
	
	void OnGUI() {
		Vector2 Center = new Vector2(Screen.width/2, Screen.height/2);
		Vector2 MenuSize = new Vector2(400,600);
		Vector2 MenuRoot = new Vector2(Center.x - MenuSize.x/2, Center.y - MenuSize.y/2);
		Vector2 ButtonRoot = MenuRoot + new Vector2(40,100);
		Vector2 ButtonSize = new Vector2(320,64);
		float buttonSpace = ButtonSize.y + 32;
		
		if (state <= MenuState.Options)
			GUI.Box(new Rect(Center.x - MenuSize.x/2, Center.y - MenuSize.y/2, MenuSize.x, MenuSize.y),"Mar",skin.box);
		
		switch (state) {
		case MenuState.Main:
			if (GUI.Button(new Rect(ButtonRoot.x, ButtonRoot.y, ButtonSize.x, ButtonSize.y),
				"Tutorial", skin.button)) {
				state = MenuState.Tutorial0;
			}
			if (GUI.Button(new Rect(ButtonRoot.x, ButtonRoot.y + buttonSpace, ButtonSize.x, ButtonSize.y),
				"Play", skin.button)) {
				Application.LoadLevel(1);
			}
			if (GUI.Button(new Rect(ButtonRoot.x, ButtonRoot.y + buttonSpace*2, ButtonSize.x, ButtonSize.y),
				"Credits", skin.button)) {
				state = MenuState.Credits;
			}
			if (GUI.Button(new Rect(ButtonRoot.x, ButtonRoot.y + buttonSpace*3, ButtonSize.x, ButtonSize.y),
				"Options", skin.button)) {
				state = MenuState.Options;
			}
			if (GUI.Button(new Rect(ButtonRoot.x, ButtonRoot.y + buttonSpace*4, ButtonSize.x, ButtonSize.y),
				"Exit", skin.button)) {
				Debug.Log("Exiting");
				Application.Quit();
			}
			break;
		case MenuState.Credits:
			GUI.TextArea(new Rect(ButtonRoot.x, ButtonRoot.y, ButtonSize.x, ButtonSize.y + buttonSpace*3),
				"Lead Designer: Ian Ooi\n\nLead Developer: Matthew McMullan\n\n" +
				"Audio Director: Leo Antlyes\n\nWatercolor Technique Implemented as described by: Adrien Bousseau et al. npar 2006: " +
				"'Interactive watercolor rendering with temporal coherence and abstraction'\n\nCopyright (C) 2013\nMatthew McMullan and Ian Ooi", skin.textArea);
			
			if (GUI.Button(new Rect(ButtonRoot.x, ButtonRoot.y + buttonSpace*4, ButtonSize.x, ButtonSize.y),
				"Back", skin.button)) {
				state = MenuState.Main;
			}
			break;
		case MenuState.Options:
			GUI.Label(new Rect(ButtonRoot.x, ButtonRoot.y, ButtonSize.x, ButtonSize.y),
				"Audio Volume", skin.label);
			AudioListener.volume = GUI.HorizontalSlider(
				new Rect(ButtonRoot.x, ButtonRoot.y + 30, ButtonSize.x, ButtonSize.y),
				AudioListener.volume, 0.0f, 1.0f);
			if (GUI.Button(new Rect(ButtonRoot.x, ButtonRoot.y + buttonSpace*4, ButtonSize.x, ButtonSize.y),
				"Back", skin.button)) {
				state = MenuState.Main;
			}
			break;
		case MenuState.Tutorial0:
			if (GUI.Button(new Rect(Screen.width/20, Screen.height - 200, 300,150),
				"Mar are hapless little geometric creatures\n" +
				"that need your help to find their way home.\n\n" +
				"Mar Balls will come from one area and are\n" +
				"predictable, but not very smart.", skin.button)) {
				state = MenuState.Tutorial1;
			}
			break;
		case MenuState.Tutorial1:
			if (GUI.Button(new Rect(Screen.width/20, 200, 300,150),
				"Mar come in several shapes and materials.\n\n" +
				"These rubber ones are very active and can\n" +
				"fall from great distances.\n\n" +
				"Magnetic mar like to stick to green magnets,\n" +
				"but quickly get distracted at a distance.", skin.button)) {
				state = MenuState.Tutorial2;
			}
			break;
		case MenuState.Tutorial2:
			if (GUI.Button(new Rect(Screen.width - 300 - Screen.width/20, 200, 300,150),
				"Other Mar come as blocks and ramps.\n\n" +
				"These Mar are more social and direct\n" +
				"other Mar to change direction or go up.\n\n" +
				"These Mar retain their material properties,\n" +
				"and can stick to things or bounce if needed.", skin.button)) {
				state = MenuState.Tutorial3;
			}
			break;
		case MenuState.Tutorial3:
			if (GUI.Button(new Rect(Screen.width - 300 - Screen.width/20, 200, 300,150),
				"When you're ready, click start to see how\n" +
				"you can transform Mar shapes and materials\n" +
				"to get them home.\n\n", skin.button)) {
				state = MenuState.Main;
			}
			break;
		}
	}
}
