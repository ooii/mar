﻿using UnityEngine;
using System.Collections;

public class Scroller : MonoBehaviour {
	public float dx = 60.0f, dy = 60.0f;
	public float marginPercent = 0.04f;
	public GameObject Background = null;
	public float padLeft = 0.0f, padRight = 0.0f, padTop = 0.0f, padBottom = 0.0f;
	
	private float camWidth = 0.0f;
	private float camHeight = 0.0f;
	private float pMarginX, pMarginY;
	
	private float cbLeft, cbRight, cbTop, cbBottom;
	// Use this for initialization
	void Start () {
		// sanitize inputs
		if (Background == null) {
			Debug.LogError("No object set as bounds for the camera!");
			Debug.Break();
		}
		
		Vector3 zero = Camera.main.ScreenToWorldPoint(new Vector3(0,0,0));
		Vector3 far = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width,Screen.height,0));
		camWidth = Mathf.Abs(zero.x - far.x);
		camHeight = Mathf.Abs(zero.y - far.y);
		
		pMarginX = marginPercent * Screen.width;
		pMarginY = marginPercent * Screen.height;
		
		float bgLeft, bgRight, bgTop, bgBottom;
		Bounds bounds = Background.GetComponent<Collider>().bounds;
		
		bgLeft = bounds.center.x - bounds.extents.x - padLeft;
		bgRight = bounds.center.x + bounds.extents.x + padRight;
		bgTop = bounds.center.y + bounds.extents.y + padTop;
		bgBottom = bounds.center.y - bounds.extents.y - padBottom;
		
		cbLeft = Mathf.Ceil( bgLeft + camWidth/2 );
		cbRight = Mathf.Floor(bgRight - camWidth/2);
		cbTop = Mathf.Floor(bgTop - camHeight/2);
		cbBottom = Mathf.Ceil( bgBottom + camHeight/2 );
	}
	
	// Update is called once per frame
	void Update () {
		// move in a direction
		if (Input.mousePosition.y <= pMarginY) {
			// down
			float speed = Mathf.Min(dy, dy * (pMarginY - Input.mousePosition.y)/pMarginY );
			float distance = Time.deltaTime * speed;
			Camera.main.transform.position += new Vector3(0,-distance,0);
		} else if (Input.mousePosition.y >= Screen.height - pMarginY) {
			// up
			float speed = Mathf.Min(dy, dy * (pMarginY - (Screen.height - Input.mousePosition.y))/ pMarginY );
			float distance = Time.deltaTime * speed;
			Camera.main.transform.position += new Vector3(0, distance,0);
		}
		if (Input.mousePosition.x <= pMarginX) {
			// left
			float speed = Mathf.Min(dx, dx * (pMarginX - Input.mousePosition.x)/pMarginX);
			float distance = Time.deltaTime * speed;
			Camera.main.transform.position += new Vector3(-distance,0,0);
		} else if (Input.mousePosition.x >= Screen.width - pMarginX) {
			// right
			float speed = Mathf.Min(dx, dx * (pMarginX - (Screen.width - Input.mousePosition.x))/ pMarginX );
			float distance = Time.deltaTime * speed;
			Camera.main.transform.position += new Vector3(distance,0,0);
		}
		
		// bound the camera
		Camera.main.transform.position = new Vector3(
			Mathf.Min (Mathf.Max(Camera.main.transform.position.x,cbLeft), cbRight),
			Mathf.Min (Mathf.Max(Camera.main.transform.position.y,cbBottom), cbTop),
			Camera.main.transform.position.z
			);
	}
}
