﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class Watercolor : MonoBehaviour {
	
	public Shader shader = null;
	public Texture PaperTex = null;
	public Texture TFlowTex = null;
	public Texture PDispTex = null;
	
	public float PaperB = 1.0f;
	public float TFlowB = 1.0f;
	public float PDispB = 1.0f;
    public float EdgeDarkeningScale = 1.0f;
    public float WobbleScale = 3.0f;
    public float PaperGradientRadius = 2.0f;
    public float ColorGradientRadius = 1.0f;

	static Material m_Material = null;
	protected Material material {
		get {
			if (m_Material == null) {
				m_Material = new Material(shader);
				m_Material.hideFlags = HideFlags.DontSave;
				m_Material.SetTexture("_PaperTex", PaperTex);
				m_Material.SetTexture("_TFlowTex", TFlowTex);
				m_Material.SetTexture("_PDispTex", PDispTex);
				
				m_Material.SetFloat("_PaperB", PaperB);
				m_Material.SetFloat("_TFlowB", TFlowB);
				m_Material.SetFloat("_PDispB", PDispB);
                m_Material.SetFloat("_EdgeScale", EdgeDarkeningScale);
                m_Material.SetFloat("_WobbleScale", WobbleScale);
                m_Material.SetFloat("_PaperGradientRadius", PaperGradientRadius);
                m_Material.SetFloat("_ColorGradientRadius", ColorGradientRadius);
			}
			return m_Material;
		} 
	}
	protected void OnDisable() {
		if( m_Material ) {
			DestroyImmediate( m_Material );
		}
	}
	
	// Use this for initialization
	void Start () {
		// Disable if we don't support image effects
		if (!SystemInfo.supportsImageEffects) {
            Debug.Log("No image effects available");
			enabled = false;
			return;
		}
		// Disable if the shader can't run on the users graphics card
		if (!shader || !material.shader.isSupported) {
            Debug.Log("Can't run on available graphics card");
			enabled = false;
			return;
		}
	}
	
	// Called by the camera to apply the image effect
	void OnRenderImage (RenderTexture source, RenderTexture destination) {		
		int rtW = source.width;
		int rtH = source.height;
		RenderTexture buffer = RenderTexture.GetTemporary(rtW, rtH, 0);
		
		Graphics.BlitMultiTap (source, destination, material, new Vector2(0,0));
		
		RenderTexture.ReleaseTemporary(buffer);
	}
}
