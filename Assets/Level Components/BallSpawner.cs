﻿using UnityEngine;
using System.Collections;

public class BallSpawner : MonoBehaviour {
    public float spawn_dt = 1.0f;
    public int num_balls = 10;
    public MarController first_ball;
    public int num_spawned = 0;
    private float dt = 0.0f;
    public float speed = 5.0f;
	public float spawn_start_dt = 2.0f;
	private bool started = false;

    void Start() {
        //first_ball.StopMoving();
        Debug.Log(first_ball.GetType());
    }
	
	// Update is called once per frame
	void Update () {
        dt += Time.deltaTime;
		if (!started) {
			if (dt >= spawn_start_dt) {
				dt -= spawn_start_dt;
				started = true;
			} else {
				return;
			}
		}
        if (num_spawned < num_balls) {
            if (dt >= spawn_dt) {
                spawnBall();
                dt -= spawn_dt;
            }
        }
	}

    void spawnBall() {
        MarController newball = (MarController) Instantiate(first_ball, this.transform.position, Quaternion.identity);
        newball.rigidbody.velocity = new Vector3 (speed,0,0);
		//Debug.Log(newball.GetType());
        //newball.StartMoving();
        num_spawned += 1;
    }
}
