﻿using UnityEngine;
using System.Collections;

public class DeathPit : MonoBehaviour {
	public int died = 0;
	private float lastCall = 0;
	
	void OnTriggerEnter(Collider other) {
        if (other.transform.GetComponent<MarController>() != null) {
        	if (other.gameObject.GetComponent<MarController>().material != MarMaterial.dead) {
	            if (lastCall != Time.time) died += 1;
			}
			Destroy(other.transform.gameObject);
        }
		lastCall = Time.time;
    }
}
