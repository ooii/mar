﻿using UnityEngine;
using System.Collections;

public class WinConditions : MonoBehaviour {
    public int minNumSaved = 10;
    public BallSpawner start = null;
    public int numSaved = 0;
	private float lastCall = 0;
	
	public void Start() {
		if (start == null)
	    {
	        Debug.LogError("Parameter cannot be null: start");
			Debug.Break();
	    }
	}
	
	// Update is called once per frame
	void Update () {
	    if (numSaved >= minNumSaved) {
            LevelTransition();
        }
	}

    void OnTriggerEnter(Collider other) {
		if (lastCall == Time.time) return;
		lastCall = Time.time;
		
        if (other.transform.GetComponent<MarController>() != null) {
        	if (other.gameObject.GetComponent<MarController>().material != MarMaterial.dead) {
	            numSaved += 1;
				//if (numSaved >= minNumSaved) {
				//	foreach (Light light in FindObjectsOfType(typeof(Light))) {
				//		if (light.type == LightType.Spot) {
				//			light.enabled = true;
				//		} else {
				//			light.enabled = false;
				//		}
				//	}
				//	Camera.main.backgroundColor = new Color(0,0,0,0);
				//}
				Debug.Log("got one!");
			}
			Destroy(other.transform.gameObject);
        }
    }
    
    bool EnoughSaved() {
        return numSaved >= minNumSaved;
    }
    
    int Surplus() {
        return Mathf.Max(0, numSaved - minNumSaved);
    }
    
    bool OCDBonus() {
        return numSaved == start.num_balls;
    }

    void LevelTransition() {

    }
}
