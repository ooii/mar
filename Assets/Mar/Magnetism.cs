﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Magnetism : MonoBehaviour {
	public float G = 0.1f;
	public Dictionary<GameObject,int> intersecting = new Dictionary<GameObject, int>();
	public GameObject parent;
	
	void Start() {
		parent = this.gameObject.transform.parent.gameObject;
	}
	
	void Update() {
		parent.rigidbody.AddForce(GetNetForce());
	}
	
	public Vector3 GetNetForce() {
		Vector3 gforce = new Vector3(0,0,0);
		if (parent.GetComponent<MarController>().material != MarMaterial.magnetic) {
			return gforce;
		}
		
		float pm = parent.GetComponent<Rigidbody>().mass;
		if (parent.GetComponent<MarController>().shape == MarShape.ball) {
			pm = 1;
		} else {
			pm = 2000;
		}
		
		foreach (GameObject i in intersecting.Keys) {
			Vector3 oPoint = i.GetComponent<BoxCollider>().ClosestPointOnBounds(parent.transform.position);
			Vector3 AB = oPoint-parent.transform.position;
			float r2 = (oPoint-parent.transform.position).sqrMagnitude;
			float om = 1000;
			float f = Time.deltaTime * G * pm * om / r2;
			//Debug.Log(AB * f);
			gforce = gforce + (f * AB);
		}
		return gforce;
	}
	
	void OnTriggerEnter(Collider other) {
		// object is magnetic
		if (other.gameObject.layer != 9) {
			return;
		}
		
		if (intersecting.ContainsKey(other.gameObject)) {
			intersecting[other.gameObject]++;
		} else {
			intersecting.Add(other.gameObject,1);
		}
	}
	void OnTriggerExit(Collider other) {
		// object is magnetic
		if (other.gameObject.layer != 9) {
			return;
		}
		
		if (intersecting.ContainsKey(other.gameObject)) {
			intersecting[other.gameObject]--;
			if (intersecting[other.gameObject] <= 0) {
				intersecting.Remove(other.gameObject);
			}
		}
	}
}
