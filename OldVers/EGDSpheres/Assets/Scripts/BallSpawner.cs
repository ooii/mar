﻿using UnityEngine;
using System.Collections;

public class BallSpawner : MonoBehaviour {
    public float spawn_dt = 1.0f;
    public int num_balls = 10;
    public MarController first_ball;
    public int num_spawned = 0;
    private float dt = 0.0f;
    public float speed = 5.0f;

    void Start() {
        //first_ball.StopMoving();
        Debug.Log(first_ball.GetType());
    }
	
	// Update is called once per frame
	void Update () {
        dt += Time.deltaTime;
        if (num_spawned < num_balls) {
            if (dt >= spawn_dt) {
                spawnBall();
                dt = 0.0f;
            }
        }
	}

    void spawnBall() {
        MarController newball = (MarController) Instantiate(first_ball, this.transform.position, Quaternion.identity);
        newball.rigidbody.velocity = new Vector3 (speed,0,0);
		//Debug.Log(newball.GetType());
        //newball.StartMoving();
        num_spawned += 1;
    }
}
