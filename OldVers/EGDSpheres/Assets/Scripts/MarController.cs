﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public enum MarDirection {
	left,
	right
};

#region Material Storage Classes
/******************************************************************************
 * Enumerates the different materials that any Mar Ball can be made out of.
 * ***************************************************************************/
[System.Serializable]
public enum MarMaterial {
	normal,
	bouncy,
	dead,
	magnetic
};

/******************************************************************************
 * Contains the particular properties of any one Mar Ball material.
 * ***************************************************************************/
[System.Serializable]
public class MarMaterialProperties {
	public Material color;
	public float angularDrag;
	public float bounciness;
	public float z;
}

/******************************************************************************
 * Contains an instance of MarMaterialProperties for each material defined by
 * MarMaterial.  Mainly for organization in the editor window.
 * ***************************************************************************/
[System.Serializable]
public class MarMaterials {
	public MarMaterialProperties normal, bouncy, dead, magnetic;
};
#endregion
#region Shape Storage Classes
/******************************************************************************
 * Enumerates the different shapes that any Mar Ball can be.
 * ***************************************************************************/
[System.Serializable]
public enum MarShape {
    ball,
    block,
    leftRamp,
    rightRamp
};

/******************************************************************************
 * Contains the particular properties of any one Mar Ball shape.
 * ***************************************************************************/
[System.Serializable]
public enum MarCollider {
	sphere,
	box,
	mesh
};

[System.Serializable]
public class MarShapeProperties {
	public Mesh mesh;
	public PhysicMaterial material;
	public float mass;
	public MarCollider collider;
	public bool collidable;
}

/******************************************************************************
 * Contains an instance of MarShapeProperties for each shape defined by
 * MarShape.  Mainly for organization in the editor window.
 * ***************************************************************************/
[System.Serializable]
public class MarShapes {
	public MarShapeProperties ball, block, leftRamp, rightRamp;
};
#endregion

[RequireComponent (typeof (Rigidbody))]
[RequireComponent (typeof (MeshFilter))]
[RequireComponent (typeof (MeshRenderer))]
[RequireComponent (typeof (SphereCollider))]
[RequireComponent (typeof (BoxCollider))]
[RequireComponent (typeof (MeshCollider))]
public class MarController : MonoBehaviour {
	#region Public Properties
	public float crackSpeed;
	public MarDirection direction;
	public MarShape shape;
	public MarShapes shapeProperties;
	public MarMaterial material;
	public MarMaterials materialProperties;
	#endregion
	#region Shortcuts To Components
	private MeshFilter meshFilter;
	private MeshRenderer meshRenderer;
	private SphereCollider sphereCollider;
	private BoxCollider boxCollider;
	private MeshCollider meshCollider;
	#endregion
	private float deadTime = -1.0f;
	
	// Use this for initialization
	void Start () {
		meshFilter = GetComponent<MeshFilter>();
		meshRenderer = GetComponent<MeshRenderer>();
		sphereCollider = GetComponent<SphereCollider>();
		boxCollider = GetComponent<BoxCollider>();
		meshCollider = GetComponent<MeshCollider>();
		SetShape(shape);
		SetMaterial(material);
		
		Physics.IgnoreLayerCollision(8,8);
		Physics.gravity = new Vector3(0, -9.81f*1.5f, 0);
	}
	
	private Vector3 lastVelocity = new Vector3(0,0,0);
	void LateUpdate() {
		/*if (shape == MarShape.ball && lastVelocity.magnitude < 0.5 && rigidbody.velocity.magnitude < 0.5) {
			rigidbody.velocity *= 3;
		}*/
		lastVelocity = rigidbody.velocity;
	}
	
	// Update is called once per frame
	void Update () {
		if (shape == MarShape.ball && material != MarMaterial.dead) {
			float power = 30;
			if (direction == MarDirection.left) {
				rigidbody.angularVelocity = new Vector3(0,0,power);
			} else {
				rigidbody.angularVelocity = new Vector3(0,0,-power);
			}
		}
		if (deadTime > 0) {
			Debug.Log(Time.time - deadTime);
			if (Time.time - deadTime >=4) {
				Destroy(gameObject);
			} else {
				renderer.material.color = new Color(renderer.material.color.r,
					renderer.material.color.g, renderer.material.color.b, (4.0f - (Time.time - deadTime))/4);
			}
		}
	}
	
	float angle = 0;
	void OnCollisionEnter(Collision other) {
		if (material != MarMaterial.bouncy && other.gameObject.layer != 12) {
			if (other.gameObject.GetComponent<MarController>() == null ||
				(other.gameObject.GetComponent<MarController>() != null && 
				other.gameObject.GetComponent<MarController>().material != MarMaterial.bouncy)) {
				if (Mathf.Abs(rigidbody.velocity.y - lastVelocity.y) >= crackSpeed) {
					Debug.Log(Mathf.Abs(rigidbody.velocity.y - lastVelocity.y) + " = " + rigidbody.velocity.y + " - " + lastVelocity.y);
					SetMaterial(MarMaterial.dead);
					deadTime = Time.time;
				}
			}
		}
		if (shape == MarShape.ball) {
			if (other.gameObject.layer == 10 && material != MarMaterial.magnetic) {
				if (direction == MarDirection.left && transform.position.x > other.transform.position.x) {
					direction = MarDirection.right;
				} else if (direction == MarDirection.right && transform.position.x < other.transform.position.x) {
					direction = MarDirection.left;
				}
			} else if (other.gameObject.layer == 11 || other.gameObject.layer == 12) {
				float dist = (other.contacts[0].otherCollider.bounds.center -
							  other.contacts[0].thisCollider.bounds.center).magnitude;
				//Debug.Log(dist);
				if (dist > 0.96) {
					if (direction == MarDirection.left) {
						direction = MarDirection.right;
					} else {
						direction = MarDirection.left;
					}
				}
			} else if (material == MarMaterial.magnetic) {
				float vdot = Vector3.Dot(other.contacts[0].normal,lastVelocity.normalized);
				if (vdot < -0.9) {
					if (direction == MarDirection.left) {
						direction = MarDirection.right;
					} else {
						direction = MarDirection.left;
					}
				}
			}
		}
	}
	void CalculateRampAngle() {
		Vector3 forces = GetComponentInChildren<Magnetism>().GetNetForce() + rigidbody.mass * Physics.gravity;
		angle = 0;
		if (Mathf.Abs(forces.y) > Mathf.Abs(forces.x) && forces.y > 0) {
			angle = 180;
		} else if (Mathf.Abs(forces.y) < Mathf.Abs(forces.x)) {
			if (forces.x > 0) {
				angle = 90;
			} else {
				angle = 270;
			}
		}
	}
	void OnMouseDown() {
		if (material == MarMaterial.dead) return;
		// verify click
		RaycastHit hit = new RaycastHit();
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		if (!Physics.Raycast(ray, out hit)) return;
		if (hit.collider != sphereCollider &&
			hit.collider != boxCollider &&
			hit.collider != meshCollider) return;
		
		HUD hud = Camera.main.GetComponent<HUD>();
		Operation operation = hud.operation;
		switch (operation) {
		case Operation.MatNormal:
			if (material == MarMaterial.normal) return;
			SetMaterial(MarMaterial.normal);
			break;
		case Operation.MatBouncy:
			if (material == MarMaterial.bouncy) return;
			SetMaterial(MarMaterial.bouncy);
			break;
		case Operation.MatMagnetic:
			if (material == MarMaterial.magnetic) return;
			SetMaterial(MarMaterial.magnetic);
			break;
		case Operation.ShpSphere:
			if (shape == MarShape.ball) return;
			SetShape(MarShape.ball);
			break;
		case Operation.ShpBox:
			if (shape == MarShape.block) return;
			SetShape(MarShape.block);
			break;
		case Operation.ShpRamp:
			if (shape == MarShape.leftRamp || shape == MarShape.rightRamp) return;
			CalculateRampAngle();
			if (angle < 180) 
				SetShape(MarShape.leftRamp);
			else
				SetShape(MarShape.rightRamp);
			break;
		case Operation.ShpRampRight:
			if (shape == MarShape.leftRamp || shape == MarShape.rightRamp) return;
			CalculateRampAngle();
			if (angle < 180) 
				SetShape(MarShape.rightRamp);
			else
				SetShape(MarShape.leftRamp);
			break;
		case Operation.none:
			return;
		};
		hud.Use();
	}
	
	#region Core Material Functions
	void SetMaterial(MarMaterial newMaterial) {
		material = newMaterial;
		switch (newMaterial) {
		case MarMaterial.normal:
			ApplyMaterial(materialProperties.normal);
			break;
		case MarMaterial.bouncy:
			ApplyMaterial(materialProperties.bouncy);
			break;
		case MarMaterial.dead:
			ApplyMaterial(materialProperties.dead);
			break;
		case MarMaterial.magnetic:
			ApplyMaterial(materialProperties.magnetic);
			break;
		};
	}
	
	void ApplyMaterial(MarMaterialProperties newMaterial) {
		renderer.material = newMaterial.color;
		transform.position = new Vector3(transform.position.x, transform.position.y, newMaterial.z);
		meshRenderer.materials[0] = newMaterial.color;
		rigidbody.angularDrag = newMaterial.angularDrag;
		
		sphereCollider.material.bounciness = newMaterial.bounciness;
		boxCollider.material.bounciness = newMaterial.bounciness;
		meshCollider.material.bounciness = newMaterial.bounciness;
	}
	#endregion
	#region Core Shape Functions
	void SetShape(MarShape newShape) {
		shape = newShape;
		switch (newShape) {
		case MarShape.ball:
			ApplyShape(shapeProperties.ball);
			break;
		case MarShape.block:
			ApplyShape(shapeProperties.block);
			break;
		case MarShape.leftRamp:
			ApplyShape(shapeProperties.leftRamp);
			break;
		case MarShape.rightRamp:
			ApplyShape(shapeProperties.rightRamp);
			break;
		}
	}
	
	void ApplyShape(MarShapeProperties newShape) {
		if (newShape.collidable) {
			if (material == MarMaterial.magnetic)
				gameObject.layer = 12;
			else
				gameObject.layer = 11;
		} else {
			gameObject.layer = 8;
		}
		
		meshFilter.mesh = newShape.mesh;
		
		rigidbody.velocity *= (rigidbody.mass / newShape.mass);
		//	new Vector3(rigidbody.velocity.x * (rigidbody.mass / newShape.mass), rigidbody.velocity.y, 0);
		rigidbody.angularVelocity *= (rigidbody.mass / newShape.mass);
		rigidbody.rotation = Quaternion.AngleAxis(angle,new Vector3(0,0,1));//Quaternion.identity;
		rigidbody.mass = newShape.mass;
		
		// collider swap
		meshCollider.sharedMesh = newShape.mesh;
		sphereCollider.enabled = newShape.collider == MarCollider.sphere;
		boxCollider.enabled = newShape.collider == MarCollider.box;
		meshCollider.enabled = newShape.collider == MarCollider.mesh;
		sphereCollider.material = newShape.material;
		boxCollider.material = newShape.material;
		meshCollider.material = newShape.material;
		
		SetMaterial(material);
	}
	#endregion
}
