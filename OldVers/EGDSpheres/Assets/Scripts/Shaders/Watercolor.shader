﻿Shader "Custom/Watercolor" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
	}
	SubShader {
		 Pass {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
        #pragma target 3.0
		#include "UnityCG.cginc"
		sampler2D _MainTex;
        sampler2D _PaperTex;
        sampler2D _TFlowTex;
        sampler2D _PDispTex;
        
        // scaling factors
        float _PaperB;
        float _TFlowB;
        float _PDispB;
        float _EdgeScale;
        float _WobbleScale;
        
		half4 _MainTex_TexelSize;
        half4 _PaperTex_TexelSize;
        half4 _TFlowTex_TexelSize;
        half4 _PDispTex_TexelSize;
        
        float _PaperGradientRadius;
        float _ColorGradientRadius;
		
		struct v2f {
			float4 pos : POSITION;
			half2 uv : TEXCOORD0; 
		};
	
		v2f vert( appdata_img v ) {
			v2f o;
			o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
			o.uv = v.texcoord;
			return o;
		}
		
		float4 color_modify(float4 color, float d) {
            //return color * d;
            return color - (color - (color * color)) * (d - 1.0f);
        }

        float calculateIntensity(float4 color) {
            float intensity = 0.2126 * color.r + 0.7152 * color.g + 0.0722 * color.b;
            return intensity;
        }
        
		float4 affectPigmentDensity(half4 color, half2 uv) {
            // turbulent flow
            half4 oColor = color_modify(color, 1.0f + _TFlowB * (calculateIntensity(tex2D(_TFlowTex, uv))) - 0.5f);
            // pigment dispersion
            oColor = color_modify(oColor, 1.0f + _PDispB * (calculateIntensity(tex2D(_PDispTex, uv))) - 0.5f);
            // paper variations                                            
            oColor = color_modify(oColor, 1.0f + _PaperB * (calculateIntensity(tex2D(_PaperTex, uv))) - 0.5f);
            return oColor;
        }
        
        half gradientIntensity(half2 uv, half radius) {
        	half3 gx = abs(tex2D(_MainTex, uv - (half2(_MainTex_TexelSize.x,0) * half2(radius))) - tex2D(_MainTex, uv + (half2(_MainTex_TexelSize.x,0) * half2(radius))));
			half3 gy = abs(tex2D(_MainTex, uv - (half2(0,_MainTex_TexelSize.y) * half2(radius))) - tex2D(_MainTex, uv + (half2(0,_MainTex_TexelSize.y) * half2(radius))));
			half3 g = (gx * gx) + (gy * gy);
            g = sqrt(g);
            return length(g);
        }
        
        float4 gradient_x(half2 uv, half radius, sampler2D lookupTex, half4 lookupTexelSize) {
        	float4 gx = abs(tex2D(lookupTex, uv - (half2(lookupTexelSize.x,0) * radius)) - tex2D(lookupTex, uv + (half2(lookupTexelSize.x,0) * radius)));
            gx = normalize(gx);
            return gx.x;
        }

        float4 gradient_y(half2 uv, half radius, sampler2D lookupTex, half4 lookupTexelSize) {
			float4 gy = abs(tex2D(lookupTex, uv - (half2(0,lookupTexelSize.y) * radius)) - tex2D(lookupTex, uv + (half2(0,lookupTexelSize.y) * radius)));
            gy = normalize(gy);
            return gy.y;
        }

        half2 wobbledCoordinates(half2 uv, float gx, float gy) {
            half2 offset_val = _MainTex_TexelSize.xy * float2(gx, gy);
            offset_val *= half2(_WobbleScale);
            offset_val += uv;
            return  clamp(offset_val, 0.0f, 1.0f);
        }
        
		float4 frag(v2f i) : COLOR {
            // calculate gradient and gradient intensity
            float paper_gx = gradient_x(i.uv, _PaperGradientRadius, _PaperTex, _PaperTex_TexelSize);
            float paper_gy = gradient_y(i.uv, _PaperGradientRadius, _PaperTex, _PaperTex_TexelSize);

            // apply paper effects (wobbling, dry brush)
            // get the color, offset by the wobbling
            half2 wobbledCoords = wobbledCoordinates(i.uv, paper_gx, paper_gy);
            float4 oColor = tex2D(_MainTex, wobbledCoords); 

            half gi = gradientIntensity(wobbledCoords, _ColorGradientRadius);

            // apply edge darkening
            float edge_d = gi * _EdgeScale;
            oColor = color_modify(oColor, edge_d);
            // apply pigment density variation
            oColor = affectPigmentDensity(oColor, i.uv);
            
            return oColor;
		}
		
		ENDCG
		}
	} 
	FallBack "Diffuse"
}
